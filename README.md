# How to use locally:
These tests can either be run from this project, by placing your sources where they're expected to be, or by downloading tests and adding them to your own pr 

For CLion users, you can also run tests using the gui (probably other IDEs can do this too, but I'm only familiar with CLion).


## The recommended method: clone (requires git)
0. Clone:
```bash
$ git clone https://gitlab.com/intro-to-inline-and-void-pointers/ex1-public.git
```
1. Copy your sources to the relevant source directories ([mtm_map](https://gitlab.com/intro-to-inline-and-void-pointers/ex1-public/-/tree/master/src/chess/mtm_map)] or [chess](https://gitlab.com/intro-to-inline-and-void-pointers/ex1-public/-/tree/master/src/chess))
2. Delete / comment out `./PreLoad.cmake` (unless you prefer ninja, then install ninja-build).
3. Run `./build.sh`. The build may fail if you are not using a Fedora-based environment:
```bash
$ ./build.sh
-- The C compiler identification is GNU 11.1.1
-- The CXX compiler identification is GNU 11.1.1
-- Detecting C compiler ABI info
-- Detecting C compiler ABI info - done
-- Check for working C compiler: /usr/bin/cc - skipped
-- Detecting C compile features
-- Detecting C compile features - done
-- Detecting CXX compiler ABI info
-- Detecting CXX compiler ABI info - done
-- Check for working CXX compiler: /usr/bin/c++ - skipped
-- Detecting CXX compile features
-- Detecting CXX compile features - done
-- Found Python: /usr/bin/python3.9 (found version "3.9.5") found components: Interpreter
-- Looking for pthread.h
-- Looking for pthread.h - found
-- Performing Test CMAKE_HAVE_LIBC_PTHREAD
-- Performing Test CMAKE_HAVE_LIBC_PTHREAD - Failed
-- Looking for pthread_create in pthreads
-- Looking for pthread_create in pthreads - not found
-- Looking for pthread_create in pthread
-- Looking for pthread_create in pthread - found
-- Found Threads: TRUE
-- Configuring done
CMake Error at CMakeLists.txt:111 (add_library):
  Cannot find source file:

      src/chess/chessSystem.c

        Tried extensions .c .C .c++ .cc .cpp .cxx .cu .m .M .mm .h .hh .h++ .hm
          .hpp .hxx .in .txx .f .F .for .f77 .f90 .f95 .f03 .ispc


CMake Error at CMakeLists.txt:25 (add_library):
  Cannot find source file:

      src/chess/mtm_map/map.c

        Tried extensions .c .C .c++ .cc .cpp .cxx .cu .m .M .mm .h .hh .h++ .hm
          .hpp .hxx .in .txx .f .F .for .f77 .f90 .f95 .f03 .ispc


CMake Error at CMakeLists.txt:111 (add_library):
  No SOURCES given to target: chess_libmap


CMake Error at CMakeLists.txt:45 (add_library):
  No SOURCES given to target: own_chess


CMake Error at CMakeLists.txt:25 (add_library):
  No SOURCES given to target: own_map


CMake Generate step failed.  Build files cannot be regenerated correctly.
ninja: error: loading 'build.ninja': No such file or directory
```
4. Add your chess and map sources, i.e.
```diff
$ git diff
diff --git a/CMakeLists.txt b/CMakeLists.txt
index 184b33f..e7f13f5 100644
--- a/CMakeLists.txt
+++ b/CMakeLists.txt
@@ -26,11 +26,14 @@ add_library(
add_library(
        own_map
        ${MAP_SOURCE}
        # Add more sources here
+       src/chess/mtm_map/node.c
        )

set(CHESS_SOURCES
        ${CHESS_SOURCE}
        # Add more sources here
+       src/chess/tournament.c
+       src/chess/match.c
        )

set(TEST_SOURCES
        src/tests/map_test.cc
        src/tests/chess_test.cc
        # Add more sources here
+       src/tests/my_new_test.cc
        )
```
And you're done :)

You may use
* `./build.sh` to build
* `./test.sh` to run all tests
(These are useful in the shell.
If you use an IDE, after it reloads the cmake project it should let you run tests with a click of a button.
If it doesn't, you probably need to learn it better or switch to a different IDE.)


# Motivation:
1. [Learn in public](https://twitter.com/swyx/status/1009174159690264579?s=19) (the entire thread is highly recommended)
2. [Inspire others to join](https://www.youtube.com/watch?v=fW8amMCVAJQ)


# Requirements:
1. gcc
2. valgrind
3. ninja (optional, disable PreLoad.cmake to use normal "Unix Makefile")
4. pandoc (optional, for markdown-to-pdf conversion)


See previous [Merge Requests](https://gitlab.com/intro-to-inline-and-void-pointers/ex1-public/-/merge_requests?scope=all&utf8=%E2%9C%93&state=all&label_name[]=example) to see how to add your own tests for everyone else's benefit.

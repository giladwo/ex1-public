# Introduction to system programming - ex1 dry
## 2.1 - Implement `mergeSortedLists`

```C
Node mergeSortedLists(Node const list1,
                      Node const list2,
                      ErrorCode * const errorCode)
{
    // Implement here
}
```

## 2.2 - Corrections

### Correct foo implementation
#### Correctness errors:
1.  # add here

#### Conventions errors:
1.  # add here

#### Extra:
1.  # add here

### Corrected version of foo

```C
// Add here
```
